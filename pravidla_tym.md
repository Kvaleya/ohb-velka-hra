
# Věž

Vaším úkolem je postavit slavnou věž, o které se bude mluvit ještě staletí. Od Rady města jste dostali jasný plán, jak má věž vypadat. Na vás je sehnat materiál a stavbu zrealizovat. Zde je plán vaší stavby:

![](vez.png)

Na stavbu je potřebný *stavební materiál*, který získáváte na tržnici. Pro jeho zakoupení je potřeba mít dostatečné množství základních *surovin*. Surovin je 6 typů (dřevo, obilí, zlato, hlína, len, konopí), každý typ se získává na jiném stanovišti.

### Seznam potřebného materiálu a ceník tržnice

- trámy (4 klacky): 5x dřevo, 5x obilí, 5x hlína
- cihly (4 kameny): 5x hlína, 5x zlato, 5x konopí
- provazy (provázek): 5x konopí, 5x len, 5x dřevo
- vlajka (papír): 5x len, 5x zlato, 5x obilí

Každý typ materiálu je třeba zakoupit jen jednou. Tedy pokud si koupíte trámy, získáváte rovnou všechny 4 potřebné klacky.

### Pravidla stanoviště

Na každém stanovišti je popsán **úkol**, za jehož splnění získáte nějaké množství jedné suroviny. Stejný úkol nesmíte plnit víckrát bezprostředně po sobě (mezitím musíte jít plnit jiný úkol, jít na tržiště nebo ke své veži), a to i pokud se vám nepovede. Po splnění stanoviště ho uvěďte do původního stavu. Pokud přijdete na stanoviště a právě ho plní jiný tým, tak běžte na jiné stanoviště.

### Další pravidla:

