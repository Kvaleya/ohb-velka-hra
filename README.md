# Velká hra na víkendovku

Pdfka vytvoříte spuštěním `pdf.sh`.

## Struktura

Týmy mají čísla 1 až 6, kde pro tým 1 je nemožné získat surovinu co je v kartě hry v tabulce stanovišť jako první, a druhou surovinu v tabulce je pro ně těžké získat. Tým 2 nemůže získat v tabulce druhou surovinu, těžká je pro něj 3., atd. Pro pohodlí pořadí surovin z tabulky je:

1. dřevo
2. obilí
3. zlato
4. hlína
5. len
6. konopí
